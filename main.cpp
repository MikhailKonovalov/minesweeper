#include <iostream>
#include <ctime>

#include <GL/glut.h>

#include <gtest/gtest.h>

int numBomb = 9;

enum COLOR {E, W, B, R};

class Point {
public:
    int x,y;

    Point()
    {
        x = 0;
        y = 0;
    }

    Point(int _x, int _y)
    {
        this->x = _x;
        this->y = _y;
    }
};

int mainWindow, mouseState, mouseButton, bombs, level = 3;
char field[256][128];
bool firstClick, win, lose;

Point windowSize(128*level,128*level), lastGame(0,0), gamePos(100,100), lastField, mousePos, loseGame;

const COLOR bombd[16][16] =
{
    E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E,
    E, E, E, E, B, E, B, B, B, B, B, E, B, E, E, E,
    E, E, E, E, E, B, B, B, B, B, B, B, E, E, E, E,
    E, E, E, E, B, B, W, W, B, B, B, B, B, E, E, E,
    E, E, E, E, B, B, W, W, B, B, B, B, B, E, E, E,
    E, E, B, B, B, B, B, B, B, B, B, B, B, B, B, E,
    E, E, E, E, B, B, B, B, B, B, B, B, B, E, E, E,
    E, E, E, E, B, B, B, B, B, B, B, B, B, E, E, E,
    E, E, E, E, E, B, B, B, B, B, B, B, E, E, E, E,
    E, E, E, E, B, E, B, B, B, B, B, E, B, E, E, E,
    E, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, B, E, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E,
};

const COLOR flagd[16][16] =
{
    E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, R, R, E, E, E, E, E, E,
    E, E, E, E, E, E, R, R,	R, R, E, E, E, E, E, E,
    E, E, E, E, E, R, R, R, R, R, E, E, E, E, E, E,
    E, E, E, E, E, E, R, R, R, R, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, R, R, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, E, B, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, E, B, E, E, E, E, E, E,
    E, E, E, E, E, E, E, B, B, B, B, E, E, E, E, E,
    E, E, E, E, E, B, B, B, B, B, B, B, B, E, E, E,
    E, E, E, E, E, B, B, B, B, B, B, B, B, E, E, E,
    E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E,
    E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E,
};

void initGame(int x, int y);

class Button;

extern Button buttons[][128];

class Button
{
public:
    int x1;
    int y1;
    int x2;
    int y2;
    bool clickable;
    bool visible;
    bool pressed;
    bool flag;

    Button()
    {
        init();
    }

    void init()
    {
        x1 = 0;
        y1 = 0;
        x2 = 0;
        y2 = 0;
        clickable = true;
        visible = true;
        pressed = false;
        flag = false;
    }

    void draw()
    {
        if (visible)
        {
            glColor3f(0.75f, 0.75f, 0.75f);

            glBegin(GL_QUADS);
                glVertex2f(x1, y1);
                glVertex2f(x1, y2);
                glVertex2f(x2, y2);
                glVertex2f(x2, y1);

            pressed ? glColor3f(0.5f, 0.5f, 0.5f) : glColor3f(1.0f, 1.0f, 1.0f);

                glVertex2f(x1, y1);
                glVertex2f(x1, y2);
                glVertex2f(x1 + 2, y2 - 2);
                glVertex2f(x1 + 2, y1 + 2);

                glVertex2f(x1 + 2, y2 - 2);
                glVertex2f(x1, y2);
                glVertex2f(x2, y2);
                glVertex2f(x2 - 2, y2 - 2);

            pressed ? glColor3f(1.0f, 1.0f, 1.0f) : glColor3f(0.5f, 0.5f, 0.5f);

                glVertex2f(x2 - 2, y1 + 2);
                glVertex2f(x2 - 2, y2 - 2);
                glVertex2f(x2, y2);
                glVertex2f(x2, y1);

                glVertex2f(x1, y1);
                glVertex2f(x1 + 2, y1 + 2);
                glVertex2f(x2 - 2, y1 + 2);
                glVertex2f(x2, y1);
            glEnd();

            if(flag)
            {
                glBegin(GL_POINTS);
                    for(int k = 0; k < 16; k++)
                        for(int l = 0; l < 16; l++)
                        {
                            if(flagd[15 - l][k] != E)
                            {
                                switch(flagd[15 - l][k])
                                {
                                case W:
                                    glColor3f(1.0f, 1.0f, 1.0f);
                                    break;

                                case B:
                                    glColor3f(0.0f, 0.0f, 0.0f);
                                    break;

                                case R:
                                    glColor3f(1.0f, 0.0f, 0.0f);
                                    break;
                                }
                                glVertex2f(k + x1 - 0.5f, l + y1 + 1.5f);
                            }
                        }
                    glEnd();
            }
        }
    }

    void tick(int i, int j)
    {
        if(clickable)
        {
            if (mouseState == GLUT_DOWN && mousePos.x > x1 && mousePos.x < x2 && mousePos.y > y1 && mousePos.y < y2)
            {
                if(mouseButton == GLUT_LEFT_BUTTON)
                    pressed = true;

                flag = (mouseButton == GLUT_RIGHT_BUTTON) && !flag;
            }

            if(mouseState == GLUT_UP && pressed)
            {
                pressed = false;

                if(mousePos.x > x1 && mousePos.x < x2 && mousePos.y > y1 && mousePos.y < y2)
                    clickUp(i, j);
            }
        }
        else
        {
            if(mouseButton == GLUT_MIDDLE_BUTTON && mousePos.x > x1 && mousePos.x < x2 && mousePos.y > y1 && mousePos.y < y2)
            {
                buttons[i + 1][j + 1].pressed = mouseState == GLUT_DOWN && !buttons[i + 1][j + 1].flag;
                buttons[i]	  [j + 1].pressed = mouseState == GLUT_DOWN && !buttons[i]	  [j + 1].flag;
                buttons[i - 1][j + 1].pressed = mouseState == GLUT_DOWN && !buttons[i - 1][j + 1].flag;
                buttons[i + 1][j	].pressed = mouseState == GLUT_DOWN && !buttons[i + 1][j	].flag;
                buttons[i - 1][j	].pressed = mouseState == GLUT_DOWN && !buttons[i - 1][j	].flag;
                buttons[i + 1][j - 1].pressed = mouseState == GLUT_DOWN && !buttons[i + 1][j - 1].flag;
                buttons[i]	  [j - 1].pressed = mouseState == GLUT_DOWN && !buttons[i]	  [j - 1].flag;
                buttons[i - 1][j - 1].pressed = mouseState == GLUT_DOWN && !buttons[i - 1][j - 1].flag;

                if((buttons[i + 1][j + 1].flag +
                    buttons[i    ][j + 1].flag +
                    buttons[i - 1][j + 1].flag +
                    buttons[i + 1][j	].flag +
                    buttons[i - 1][j    ].flag +
                    buttons[i + 1][j - 1].flag +
                    buttons[i    ][j - 1].flag +
                    buttons[i - 1][j - 1].flag) == field[i][j] && mouseState == GLUT_UP)
                {
                    if(!buttons[i + 1][j + 1].flag)
                        buttons[i + 1][j + 1].clickUp(i + 1, j + 1);

                    if(!buttons[i    ][j + 1].flag)
                        buttons[i    ][j + 1].clickUp(i,     j + 1);

                    if(!buttons[i - 1][j + 1].flag)
                        buttons[i - 1][j + 1].clickUp(i - 1, j + 1);

                    if(!buttons[i + 1][j    ].flag)
                        buttons[i + 1][j    ].clickUp(i + 1, j	  );

                    if(!buttons[i - 1][j    ].flag)
                        buttons[i - 1][j    ].clickUp(i - 1, j	  );

                    if(!buttons[i + 1][j - 1].flag)
                        buttons[i + 1][j - 1].clickUp(i + 1, j - 1);

                    if(!buttons[i    ][j - 1].flag)
                        buttons[i    ][j - 1].clickUp(i,     j - 1);

                    if(!buttons[i - 1][j - 1].flag)
                        buttons[i - 1][j - 1].clickUp(i - 1, j - 1);
                }
            }
        }
    }

    void clickUp(int i, int j)
    {
        if(firstClick)
        {
            bombs = (gamePos.x * gamePos.y) / 8;
            initGame(i, j);
            firstClick = false;
        }

        visible = false;
        clickable = false;

        if(field[i][j] == numBomb)
        {
            for(int k = 0; k < gamePos.x; k++)
                for(int l = 0; l < gamePos.y; l++)
                {
                    buttons[k][l].visible = (field[k][l] == numBomb && buttons[k][l].flag) || (field[k][l] != numBomb && !buttons[k][l].flag) && buttons[k][l].visible;
                    buttons[k][l].clickable = (field[k][l] == numBomb && buttons[k][l].flag) || (field[k][l] != numBomb && !buttons[k][l].flag) && buttons[k][l].clickable;
                }

            loseGame.x = i;
            loseGame.y = j;
            lose = true;
        }
        else if(field[i][j] == 0 && j < gamePos.y && i < gamePos.x && j >= 0 && i >= 0)
        {
            if(buttons[i][j + 1].clickable && !buttons[i][j + 1].flag)
                buttons[i][j + 1].clickUp(i, j + 1);

            if(buttons[i][j - 1].clickable && !buttons[i][j - 1].flag)
                buttons[i][j - 1].clickUp(i, j - 1);

            if(buttons[i + 1][j + 1].clickable && !buttons[i + 1][j + 1].flag)
                buttons[i + 1][j + 1].clickUp(i + 1, j + 1);

            if(buttons[i - 1][j - 1].clickable && !buttons[i - 1][j - 1].flag)
                buttons[i - 1][j - 1].clickUp(i - 1, j - 1);

            if(buttons[i + 1][j - 1].clickable && !buttons[i + 1][j - 1].flag)
                buttons[i + 1][j - 1].clickUp(i + 1, j - 1);

            if(buttons[i - 1][j + 1].clickable && !buttons[i - 1][j + 1].flag)
                buttons[i - 1][j + 1].clickUp(i - 1, j + 1);

            if(buttons[i + 1][j].clickable && !buttons[i + 1][j].flag)
                buttons[i + 1][j].clickUp(i + 1, j);

            if(buttons[i - 1][j].clickable && !buttons[i - 1][j].flag)
                buttons[i - 1][j].clickUp(i - 1, j);
        }
    }
    
} buttons[256][128];

void initGame(int x, int y)
{
    int a,b;

    lose = false;

    if(bombs >= gamePos.x * gamePos.y)
        bombs = (gamePos.x * gamePos.y) - 1;

    for(int k = 0; k < bombs; 0)
    {
        a = rand() % gamePos.x;
        b = rand() % gamePos.y;

        if(!(x == a && y == b) && field[a][b] != numBomb)
        {
            field[a][b] = numBomb;
            k++;
        }
    }

    for(int i = 0; i <= gamePos.x; i++)
        for(int j = 0; j <= gamePos.x; j++)
        {
            field[i][j] == 0 ? field[i][j] = (field[i + 1][j]     == numBomb) +
                                             (field[i - 1][j]	  == numBomb) +
                                             (field[i + 1][j + 1] == numBomb) +
                                             (field[i - 1][j + 1] == numBomb) +
                                             (field[i + 1][j - 1] == numBomb) +
                                             (field[i - 1][j - 1] == numBomb) +
                                             (field[i]	  [j + 1] == numBomb) +
                                             (field[i]    [j - 1] == numBomb) : 0;
        }
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    glViewport(0, 0, windowSize.x, windowSize.y);
    glOrtho(0, windowSize.x, 0, windowSize.y, -1.0, 1.0);
    // Back gray draw
    glColor3f(0.75f, 0.75f, 0.75f);
    glBegin(GL_QUADS);
        glVertex2f(0, 0);
        glVertex2f(0, gamePos.y * 16 + 32);
        glVertex2f(gamePos.x * 16 + 32, gamePos.y * 16 + 32);
        glVertex2f(gamePos.x * 16 + 32, 0); 
    glEnd();
    
    if(lose) // Red-quad lose
    {
        glColor3f(1.0f, 0.0f, 0.0f);

        glBegin(GL_QUADS);
            glVertex2f(loseGame.x * 16 + 16, loseGame.y * 16 + 16);
            glVertex2f(loseGame.x * 16 + 16, loseGame.y * 16 + 32);
            glVertex2f(loseGame.x * 16 + 32, loseGame.y * 16 + 32);
            glVertex2f(loseGame.x * 16 + 32, loseGame.y * 16 + 16);
        glEnd();
    }

    glColor3f(0.5f, 0.5f, 0.5f);

    // Draw black line quads Back
    glBegin(GL_LINES);
        for(int i = 1; i < gamePos.x; i++)
        {
            glVertex2f(i * 16 + 16.5f, 15.5f);
            glVertex2f(i * 16 + 16.5f, gamePos.y * 16 + 16.5f);
        }

        for(int i = 1; i < gamePos.y; i++)
        {
            glVertex2f(15.5f, i * 16 + 15.5f);
            glVertex2f(gamePos.x * 16 + 16.5f, i * 16 + 15.5f);
        }
    glEnd();

    // Draw edging
    glColor3f(0.5f, 0.5f, 0.5f);

    glBegin(GL_QUADS);
        glVertex2f(13, 13);
        glVertex2f(13, gamePos.y * 16 + 19);
        glVertex2f(16, gamePos.y * 16 + 16);
        glVertex2f(16, 16);

        glVertex2f(16, gamePos.y * 16 + 16);
        glVertex2f(13, gamePos.y * 16 + 19);
        glVertex2f(gamePos.x * 16 + 19, gamePos.y * 16 + 19);
        glVertex2f(gamePos.x * 16 + 16, gamePos.y * 16 + 16);

    glColor3f(1.0f, 1.0f, 1.0f);

        glVertex2f(gamePos.x * 16 + 16, gamePos.y * 16 + 16);
        glVertex2f(gamePos.x * 16 + 19, gamePos.y * 16 + 19);
        glVertex2f(gamePos.x * 16 + 19, 13);
        glVertex2f(gamePos.x * 16 + 16, 16);

        glVertex2f(16, 16);
        glVertex2f(13, 13);
        glVertex2f(gamePos.x * 16 + 19, 13);
        glVertex2f(gamePos.x * 16 + 16, 16);
    glEnd();

    lastField.x = 255;
    lastField.y = 127;


    // draw lose game
    if(lose)
        for(int i = 0; i < gamePos.x; i++)
            for(int j = 0; j < gamePos.y; j++)
            {
                glPushMatrix();
                glTranslatef(16.0f * i + 16, 16.0f * j + 16, 0.0f);
                if(field[i][j] == numBomb)
                {
                    glBegin(GL_POINTS);
                        for(int k = 0; k < 16; k++)
                            for(int l = 0; l < 16; l++)
                            {
                                if(bombd[15 - l][k] != E)
                                {
                                    switch(bombd[15 - l][k])
                                    {
                                    case W:
                                        glColor3f(1.0f, 1.0f, 1.0f);
                                        break;

                                    case B:
                                        glColor3f(0.0f, 0.0f, 0.0f);
                                        break;

                                    case R:
                                        glColor3f(1.0f, 0.0f, 0.0f);
                                        break;
                                    }

                                    glVertex2f(k + 0.5f, l + 0.5f);
                                }
                            }
                    glEnd();
                }

                if(field[i][j] != numBomb && buttons[i][j].flag)
                {
                    glBegin(GL_POINTS);
                        for(int k = 0; k < 16; k++)
                            for(int l = 0; l < 16; l++)
                            {
                                if(bombd[15 - l][k] != E)
                                {
                                    switch(bombd[15 - l][k])
                                    {
                                    case W:
                                        glColor3f(1.0f, 1.0f, 1.0f);
                                        break;

                                    case B:
                                        glColor3f(0.0f, 0.0f, 0.0f);
                                        break;

                                    case R:
                                        glColor3f(1.0f, 0.0f, 0.0f);
                                        break;
                                    }

                                    glVertex2f(k + 0.5f, l + 0.5f);
                                }
                            }
                    glEnd();

                    glColor3f(1.0f, 0.0f, 0.0f);
                    
                    glBegin(GL_LINES);
                        glVertex2i(3, 13);
                        glVertex2i(15, 1);

                        glVertex2i(2, 13);
                        glVertex2i(14, 1);

                        glVertex2i(14, 13);
                        glVertex2i(2, 1);

                        glVertex2i(15, 13);
                        glVertex2i(3, 1);
                    glEnd();
                }

                glPopMatrix();
                
        }
        
    // Draw 1234567890
    for(int i = 0; i < gamePos.x; i++)
        for(int j = 0; j < gamePos.y; j++)
        {
            if(field[i][j] != numBomb && field[i][j] != 0 && !buttons[i][j].visible && !buttons[i][j].flag)
            {
                if(field[i][j] != field[lastField.x][lastField.y])
                    switch (field[i][j])
                    {
                    case 1:
                        glColor3f(0.0f, 0.0f, 1.0f);
                        break;

                    case 2:
                        glColor3f(0.0f, 0.5f, 0.0f);
                        break;

                    case 3:
                        glColor3f(1.0f, 0.0f, 0.0f);
                        break;

                    case 4:
                        glColor3f(0.0f, 0.0f, 0.5f);
                        break;

                    case 5:
                        glColor3f(0.5f, 0.0f, 0.0f);
                        break;

                    case 6:
                        glColor3f(0.0f, 0.5f, 0.5f);
                        break;

                    case 7:
                        glColor3f(1.0f, 0.0f, 0.0f);
                        break;

                    case 8:
                        glColor3f(0.4f, 0.4f, 0.4f);
                        break;

                    default:
                        glColor3f(0.0f, 0.0f, 0.0f);
                        break;
                    }

                lastField.x = i;
                lastField.y = j;

                glRasterPos2f(i * 16 + 21.5f, j * 16 + 19.5f);
                glutBitmapCharacter(GLUT_BITMAP_9_BY_15, field[i][j] + '0');

                glRasterPos2f(i * 16 + 21.5f, j * 16 + 18.5f);
                glutBitmapCharacter(GLUT_BITMAP_9_BY_15, field[i][j] + '0');

                glRasterPos2f(i * 16 + 20.5f, j * 16 + 19.5f);
                glutBitmapCharacter(GLUT_BITMAP_9_BY_15, field[i][j] + '0');

                glRasterPos2f(i * 16 + 20.5f, j * 16 + 18.5f);
                glutBitmapCharacter(GLUT_BITMAP_9_BY_15, field[i][j] + '0');

                glRasterPos2f(i * 16 + 19.5f, j * 16 + 19.5f);
                glutBitmapCharacter(GLUT_BITMAP_9_BY_15, field[i][j] + '0');

                glRasterPos2f(i * 16 + 19.5f, j * 16 + 18.5f);
                glutBitmapCharacter(GLUT_BITMAP_9_BY_15, field[i][j] + '0');
            }
        }

        for(int i = 0; i < gamePos.x; i++)
            for(int j = 0; j < gamePos.y; j++)
                buttons[i][j].draw();

    glutSwapBuffers();
}

void mouse(int button, int state, int ax, int ay)
{
    mousePos.x = ax;
    mousePos.y = windowSize.y - ay;
    mouseButton = button;
    mouseState = state;

    for(int i = 0; i < gamePos.x; i++)
        for(int j = 0; j < gamePos.y; j++)
            if (!(lose || win))
                buttons[i][j].tick(i, j);
}

void initButtons()
{
    firstClick = true;
    lose = false;

    for(int i = 0; i < 256; i++)
        for(int j = 0; j < 128; j++)
            field[i][j] = 0;

    for(int i = 0; i < gamePos.x; i++)
        for(int j = 0; j < gamePos.y; j++)
        {
            buttons[i][j].init();
            buttons[i][j].x1 = (i + 1) * 16;
            buttons[i][j].y1 = (j + 1) * 16;
            buttons[i][j].x2 = ((i + 1) * 16) + 16;
            buttons[i][j].y2 = ((j + 1) * 16) + 16;
        }
}

void timer(int = 0)
{
    int openCells = 0;

    for(int i = 0; i < gamePos.x; i++)
        for(int j = 0; j < gamePos.y; j++)
            if(!buttons[i][j].visible && !buttons[i][j].clickable)
                openCells++;

    win = openCells == (gamePos.x * gamePos.y) - bombs;

    if(win)
        for(int i = 0; i < gamePos.x; i++)
            for(int j = 0; j < gamePos.y; j++)
                buttons[i][j].flag = buttons[i][j].visible;

    display();
}

void windowResize(int x, int y)
{
    x = (int)(x / 16) * 16;
    y = (int)(y / 16) * 16;

    if(x < 64)
        x = windowSize.x;
    if(y < 32)
        y = windowSize.y;

    windowSize.x = (x / 16) * 16;
    windowSize.y = (y / 16) * 16;

    gamePos.x = windowSize.x / 16 - 2;
    gamePos.y = windowSize.y / 16 - 2;

    if(gamePos.x != lastGame.x || gamePos.y != lastGame.y)
    {
        initButtons();
        bombs = 0;

        for(int i = 0; i <= gamePos.x; i++)
            for(int j = 0; j <= gamePos.y; j++)
                field[i][j] = 0;
    }

    lastGame.x = gamePos.x;
    lastGame.y = gamePos.y;

    glutReshapeWindow(windowSize.x, windowSize.y);
}

void initLevel(int l)
{
    level = l;
    std::cout << "Level: " << level << std::endl;
    initButtons();
    windowResize(128*level, 128*level);
}

void keyboard(int key, int x, int y)
{
	switch(key) {
        case GLUT_KEY_F1:
            initLevel(1);
            break;
        case GLUT_KEY_F2:
            initLevel(2);
            break;
        case GLUT_KEY_F3:
            initLevel(3);
            break;
        case GLUT_KEY_F4:
            initLevel(4);
            break;
        case GLUT_KEY_F5:
            initLevel(5);
            break;
        case GLUT_KEY_F6:
            initLevel(6);
            break;
        case GLUT_KEY_F7:
            initLevel(7);
            break;
        case GLUT_KEY_F12:
            initButtons();
            windowResize(128*level, 128*level);
            break;

    }
}

void information()
{
    std::cout << "Change level to press F1-F7" << std::endl;
    std::cout << "Restart field to press F12" << std::endl;
    std::cout << "Level: " << level << std::endl;   
}

//TESTING

TEST(TestIntLvl, TestStartLevel)
{
    ASSERT_EQ(level, 3); 
}
TEST(TestClassPoint, TestXY)
{
    Point testPoint0;
    ASSERT_EQ(testPoint0.x, 0);
    ASSERT_EQ(testPoint0.y, 0);
    Point testPoint(3,6);
    ASSERT_EQ(testPoint.x, 3);
    ASSERT_EQ(testPoint.y, 6);
}
TEST(TestClassButton, TestConstructor)
{
    Button testButton;
    ASSERT_EQ(testButton.x1, 0);
    ASSERT_EQ(testButton.y1, 0);
    ASSERT_EQ(testButton.x2, 0);
    ASSERT_EQ(testButton.y2, 0);
    ASSERT_TRUE(testButton.clickable);
    ASSERT_TRUE(testButton.visible);
    ASSERT_FALSE(testButton.pressed);
    ASSERT_FALSE(testButton.flag);
    

}
TEST(TestFunction, TestFunctionInitButton)
{
    initButtons();
    ASSERT_TRUE(firstClick);
    ASSERT_FALSE(lose);
    for(int i = 0; i < 256; i++)
        for(int j = 0; j < 128; j++)
            ASSERT_EQ(field[i][j], 0);  

    for(int i = 0; i < gamePos.x; i++)
        for(int j = 0; j < gamePos.y; j++)
        {
            ASSERT_EQ(buttons[i][j].x1, (i + 1) * 16);
            ASSERT_EQ(buttons[i][j].y1, (j + 1) * 16);
            ASSERT_EQ(buttons[i][j].x2, ((i + 1) * 16) + 16);
            ASSERT_EQ(buttons[i][j].y2, ((j + 1) * 16) + 16);
        }

}

// END TESTING

int main(int argc, char **argv)
{
    if(argc > 1)
    {
        ::testing::InitGoogleTest(&argc, argv);
        return RUN_ALL_TESTS();
    }
    information();
    srand(std::time(NULL)); 
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(windowSize.x, windowSize.y);
    mainWindow = glutCreateWindow("Minesweeper");
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glMatrixMode(GL_PROJECTION);
    glutDisplayFunc(display);
    glutReshapeFunc(windowResize);
    glutMouseFunc(mouse);
    glutSpecialFunc(keyboard);
    glutTimerFunc(1, timer, 0);
    glutMainLoop();
    return 0;
}
